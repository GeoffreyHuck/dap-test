import { SocketDebugClient, LogLevel } from "node-debugprotocol-client";
import * as path from 'path';

const __dirname = path.resolve();
const programPath = __dirname + '/test_functions.php';

// create a client instance
const client = new SocketDebugClient({
  port: 4711,
  host: "localhost",
  logLevel: LogLevel.Verbose,
  loggerName: "My Debug Adapter Client"
});

// connect
await client.connectAdapter();


// initialize first
await client.initialize({
  adapterId: "php",
  pathFormat: 'path',

})

// tell the debug adapter to attach to a debuggee which is already running somewhere
// SpecificAttachArguments has to extend DebugProtocol.AttachRequestArguments
// await client.attach<SpecificAttachArguments>({
//   // ...
// });

await client.launch({
  program: programPath,
  runtimeArgs: ['-dxdebug.mode=debug', '-dxdebug.start_with_request=1'],
})

// await client.waitForEvent('initialized');

// set some breakpoints
await client.setBreakpoints({
  breakpoints: [
    { line: 1 },
    // { line: 2 },
    // { line: 3 },
    // { line: 4 },
  ],
  source: {
    path: programPath
  }
});

client.onTerminated(async (event) => {
  client.disconnectAdapter();
});

// listen to events such as "stopped"
const unsubscribable = client.onStopped(async (stoppedEvent) => {
  console.log('on stopped', stoppedEvent);
  if (stoppedEvent.reason === 'breakpoint' || stoppedEvent.reason === 'step') {
    const threads = await client.threads();

    for (const thread of threads) {
      const stackFrames = (await client.stackTrace({ threadId: thread.id })).stackFrames;
      
      for (const stackFrame of stackFrames) {
        const scopes = (await client.scopes({ frameId: stackFrame.id })).scopes;
    
        // const localScope = scopes.find(scope => scope.name === 'Locals')
        // const superglobalsScope = scopes.find(scope => scope.name === 'Superglobals')
        // const constantsScope = scopes.find(scope => scope.name === 'User defined constants') // Xdebug >2.3 only
    
        const localVariables = (await client.variables({ variablesReference: localScope.variablesReference })).variables;
        for (const localVariable of localVariables) {
          if (localVariable.variablesReference != 0) { // 0 is a primitive.
            const variable = (await client.variables({ variablesReference: localVariable.variablesReference })).variables;

            // TODO: Recursively.
          }
        }
      }
    }

    

    //console.log('variables', localVariables);

    // const myArray = localVariables.find(variable => variable.name === '$d')
    // const items = (await client.variables({ variablesReference: myArray.variablesReference })).variables;
    // console.log('items', items);

    // do some debugging
  } else {
    console.log('Unhandled StoppedEvent.');
  }

  // next request.
  await client.stepIn({
    threadId: 1,
    granularity: 'statement'
   });
});

// send 'configuration done' (in some debuggers this will trigger 'continue' if attach was awaited)
await client.configurationDone();

//console.log('threads', threads);

// await client.continue();

// ...
//
// // Event subscriptions can be unsubscribed
// unsubscribable.unsubscribe();
//
// // disconnect from the adapter when done
// client.disconnectAdapter();
