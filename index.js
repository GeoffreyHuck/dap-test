import { SocketDebugClient, LogLevel } from "node-debugprotocol-client";
import * as path from 'path';

const __dirname = path.resolve();
const programPath = __dirname + '/hello_world.php';

// create a client instance
const client = new SocketDebugClient({
  port: 4711,
  host: "localhost",
  logLevel: LogLevel.Verbose,
  loggerName: "My Debug Adapter Client"
});

// connect
await client.connectAdapter();


// initialize first
await client.initialize({
  adapterId: "php",
  pathFormat: 'path',

})

// tell the debug adapter to attach to a debuggee which is already running somewhere
// SpecificAttachArguments has to extend DebugProtocol.AttachRequestArguments
// await client.attach<SpecificAttachArguments>({
//   // ...
// });

await client.launch({
  program: programPath,
  runtimeArgs: ['-dxdebug.mode=debug', '-dxdebug.start_with_request=1'],
})

// await client.waitForEvent('initialized');

// set some breakpoints
await client.setBreakpoints({
  breakpoints: [
    { line: 8 },
    // { line: 2 },
    // { line: 3 },
    // { line: 4 },
  ],
  source: {
    path: programPath
  }
})

client.onTerminated(async (event) => {
  client.disconnectAdapter();
});

// listen to events such as "stopped"
const unsubscribable = client.onStopped(async (stoppedEvent) => {
  console.log('on stopped', stoppedEvent);
  if (stoppedEvent.reason === "breakpoint") {
    // we hit a breakpoint!

    const stackFrame = (await client.stackTrace({ threadId: 1 })).stackFrames[0];
    console.log('stack frame', stackFrame);
    const scopes = (await client.scopes({ frameId: stackFrame.id })).scopes;
    console.log('scopes', scopes);

    const localScope = scopes.find(scope => scope.name === 'Locals')
    const superglobalsScope = scopes.find(scope => scope.name === 'Superglobals')
    const constantsScope = scopes.find(scope => scope.name === 'User defined constants') // Xdebug >2.3 only

    const localVariables = (await client.variables({ variablesReference: localScope.variablesReference })).variables;
    console.log('variables', localVariables);

    const myArray = localVariables.find(variable => variable.name === '$d')
    const items = (await client.variables({ variablesReference: myArray.variablesReference })).variables;
    console.log('items', items);

    // do some debugging

    // continue all threads
    //await client.continue({ threadId: 1 });
  }
});

// send 'configuration done' (in some debuggers this will trigger 'continue' if attach was awaited)
await client.configurationDone();

// await client.continue();

// ...
//
// // Event subscriptions can be unsubscribed
// unsubscribable.unsubscribe();
//
// disconnect from the adapter when done

